//
//  ViewController.swift
//  Project25
//
//  Created by Ruben Dias on 19/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import UIKit
import MultipeerConnectivity

class ViewController: UICollectionViewController {

    var images = [UIImage]()
    
    var peerID = MCPeerID(displayName: UIDevice.current.name)
    var mcSession: MCSession?
    var mcAdvertiserAssistant: MCAdvertiserAssistant?
    let serviceType = "hws-project25"
    
    var sessionLabel: UILabel!
    var sessionInfoButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Selfie Share"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showConnectionPrompt))
        let textIcon = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(sendMessage))
        let cameraIcon = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(sendPicture))
        navigationItem.rightBarButtonItems = [textIcon, cameraIcon]
        
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession?.delegate = self
        
        configureSessionInfoLabel()
    }
    
    @objc func showConnectionPrompt() {
        let ac = UIAlertController(title: "Connect to others", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Host a session", style: .default, handler: startHosting))
        ac.addAction(UIAlertAction(title: "Join a session", style: .default, handler: joinSession))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(ac, animated: true)
    }
    
    func startHosting(action: UIAlertAction) {
        guard let mcSession = mcSession else { return }
        mcAdvertiserAssistant = MCAdvertiserAssistant(serviceType: serviceType, discoveryInfo: nil, session: mcSession)
        mcAdvertiserAssistant?.start()
    }

    func joinSession(action: UIAlertAction) {
        guard let mcSession = mcSession else { return }
        let mcBrowser = MCBrowserViewController(serviceType: serviceType, session: mcSession)
        mcBrowser.delegate = self
        present(mcBrowser, animated: true)
    }
    
    @objc private func sendMessage() {
        let ac = UIAlertController(title: "Message", message: nil, preferredStyle: .alert)
        ac.addTextField()
        
        ac.addAction(UIAlertAction(title: "Send", style: .default, handler: { [weak self, weak ac] _ in
            if let text = ac?.textFields?[0].text, !text.isEmpty {
                let textData = Data(text.utf8)
                self?.sendDataToPeers(textData)
            }
        }))
        
        present(ac, animated: true, completion: nil)
    }
    
    @objc func sendPicture() {
        if UIImagePickerController.isCameraDeviceAvailable(.rear) {
            let ac = UIAlertController(title: "Import Photo", message: "Please select your preferred input method", preferredStyle: .actionSheet)

            ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
            
            ac.addAction(UIAlertAction(title: "New Photo", style: .default) { [weak self] _ in
                self?.importPictureFromSource(.camera)
            })
            
            ac.addAction(UIAlertAction(title: "Camera Roll", style: .default) { [weak self] _ in
                self?.importPictureFromSource(.photoLibrary)
            })

            present(ac, animated: true)
        } else {
            importPictureFromSource(.photoLibrary)
        }
    }
    
    func importPictureFromSource(_ source: UIImagePickerController.SourceType) {
        let picker = UIImagePickerController()
        picker.sourceType = source
        picker.delegate = self
        present(picker, animated: true)
    }
    
    func configureSessionInfoLabel() {
        sessionLabel = UILabel()
        sessionLabel.translatesAutoresizingMaskIntoConstraints = false
        sessionLabel.textAlignment = .center
        sessionLabel.font = .systemFont(ofSize: 17, weight: .semibold)
        sessionLabel.text = "No active session"
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = .lightGray
        
        sessionInfoButton = UIButton(type: .infoLight)
        sessionInfoButton.translatesAutoresizingMaskIntoConstraints = false
        sessionInfoButton.addTarget(self, action: #selector(showSessionInfo), for: .touchUpInside)
        sessionInfoButton.isHidden = true
        
        view.addSubview(sessionLabel)
        view.addSubview(separatorView)
        view.addSubview(sessionInfoButton)
        NSLayoutConstraint.activate([
            sessionLabel.heightAnchor.constraint(equalToConstant: 43.5),
            sessionLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            sessionLabel.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            sessionLabel.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            
            separatorView.heightAnchor.constraint(equalToConstant: 0.5),
            separatorView.bottomAnchor.constraint(equalTo: sessionLabel.topAnchor),
            separatorView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
            separatorView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
            
            sessionInfoButton.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -20),
            sessionInfoButton.centerYAnchor.constraint(equalTo: sessionLabel.centerYAnchor)
        ])
    }
    
    @objc func showSessionInfo() {
        guard let mcSession = mcSession else { return }
        var peers = mcSession.connectedPeers.map { $0.displayName }
        
        let ac = UIAlertController(title: "Connected to...", message: peers.joined(separator: "\n"), preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(ac, animated: true, completion: nil)
    }
    
    func updateSessionLabel(_ state: MCSessionState) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            switch state {
            case .connected:
                self.sessionLabel.text = "Connected, \(self.mcSession!.connectedPeers.count) active peers"
                self.sessionInfoButton.isHidden = false
            case .connecting:
                break
            case .notConnected:
                self.sessionLabel.text = "No active session"
                self.sessionInfoButton.isHidden = true
            @unknown default:
                self.sessionLabel.text = "Session error"
                self.sessionInfoButton.isHidden = true
            }
        }
    }
    
}

extension ViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageView", for: indexPath)

        if let imageView = cell.viewWithTag(1000) as? UIImageView {
            imageView.image = images[indexPath.item]
        }

        return cell
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        
        dismiss(animated: true)

        images.insert(image, at: 0)
        collectionView.reloadData()
        
        if let imageData = image.pngData() {
            sendDataToPeers(imageData)
        }
    }
    
    func sendDataToPeers(_ data: Data) {
        guard let mcSession = mcSession, !mcSession.connectedPeers.isEmpty else { return }
        
        do {
            try mcSession.send(data, toPeers: mcSession.connectedPeers, with: .reliable)
        } catch {
            let ac = UIAlertController(title: "Send error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
}


extension ViewController: MCSessionDelegate, MCBrowserViewControllerDelegate {
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("Connected: \(peerID.displayName)")
            updateSessionLabel(state)
        case .connecting:
            print("Connecting: \(peerID.displayName)")
        case .notConnected:
            print("Not Connected: \(peerID.displayName)")
            updateSessionLabel(state)
        @unknown default:
            print("Unknown state received: \(peerID.displayName)")
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        DispatchQueue.main.async { [weak self] in
            if let message = String(data: data, encoding: .utf8) {
                let ac = UIAlertController(title: "New Message", message: message, preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self?.present(ac, animated: true, completion: nil)
            } else if let image = UIImage(data: data) {
                self?.images.insert(image, at: 0)
                self?.collectionView.reloadData()
            }
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {}

    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        print("LOGGER: Receiving data...")
    }

    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        print("LOGGER: Finished receiving data!")
    }
    
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }

    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
}
